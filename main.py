import string

from validation.email.params import EmailValidationParams

from validation.email.validator import EmailValidator


def load_words(file_path: str) -> [str]:
    result = []
    with open(file_path, 'r') as file:
        for line in file:
            result.append(line.replace('\n', '').strip())
    return result


if __name__ == '__main__':
    params = EmailValidationParams(
        letters=list(string.ascii_lowercase + string.ascii_uppercase),
        numbers=list(string.digits),
        at='@',
        dot='.',
        separators=['-', '_']
    )

    validator = EmailValidator(params)

    words = load_words('input/emails.txt')

    print('email->', 'regex->', 'state machine')

    for word in words:
        print(word, validator.regex(word), validator.validate(word))
