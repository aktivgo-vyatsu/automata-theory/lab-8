import re

from validation.email.params import EmailValidationParams


class EmailValidator:
    def __init__(self, params: EmailValidationParams):
        self.params = params

    def regex(self, inp: str) -> bool:
        reg = re.compile('([a-zA-Z0-9_.-]+)@([a-zA-Z0-9._-])+(\\.[a-zA-Z])+')
        return reg.match(inp) is not None

    def validate(self, inp: str) -> bool:
        for i in range(len(inp)):
            if inp[i] in (self.params.letters + self.params.numbers + self.params.separators + [self.params.dot]):
                return self.__username(inp[i + 1:])
            return self.__err()
        return self.__err()

    def __username(self, inp: str) -> bool:
        for i in range(len(inp)):
            if inp[i] in (self.params.letters + self.params.numbers + self.params.separators + [self.params.dot]):
                continue
            elif inp[i] == self.params.at:
                return self.__at(inp[i + 1:])
            return self.__err()
        return self.__err()

    def __at(self, inp: str):
        for i in range(len(inp)):
            if inp[i] in (self.params.letters + self.params.numbers + self.params.separators):
                return self.__domain(inp[i + 1:])
            return self.__err()

    def __domain(self, inp: str):
        for i in range(len(inp)):
            if inp[i] in (self.params.letters + self.params.numbers + self.params.separators):
                continue
            elif inp[i] == self.params.dot:
                return self.__dot(inp[i + 1:])
            return self.__err()
        return self.__err()

    def __dot(self, inp: str) -> bool:
        for i in range(len(inp)):
            if inp[i] in (self.params.letters + self.params.numbers + self.params.separators):
                return self.__after_dot(inp[i + 1:])
            return self.__err()

    def __after_dot(self, inp: str) -> bool:
        for i in range(len(inp)):
            if inp[i] in (self.params.letters + self.params.numbers + self.params.separators):
                continue
            elif inp[i] == self.params.dot:
                return self.__dot(inp[i + 1:])
            return self.__err()
        return self.__end()

    def __err(self) -> bool:
        return False

    def __end(self) -> bool:
        return True
