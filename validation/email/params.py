class EmailValidationParams:

    def __init__(self, letters: [str], numbers: [str], at: str, dot: str, separators: [str]):
        self.letters = letters
        self.numbers = numbers
        self.at = at
        self.dot = dot
        self.separators = separators
